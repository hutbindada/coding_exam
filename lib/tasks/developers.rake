namespace :developers do
  desc 'Create dummy developer’s records (a hundred records)'
  task generate_dummy: :environment do
    languages = Language.all
    programming_languages = ProgrammingLanguage.all
    100.times do |i|
      dev = Developer.new(email: "dev#{Time.now.to_i + i}@gmail.com")
      dev.languages = languages.sample([*1..languages.size].sample)
      dev.programming_languages =
        programming_languages.sample([*1..programming_languages.size].sample)
      dev.save
    end
  end
end
