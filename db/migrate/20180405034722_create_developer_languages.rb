class CreateDeveloperLanguages < ActiveRecord::Migration[5.1]
  def change
    create_table :developer_languages do |t|
      t.integer :developer_id
      t.integer :language_id
      t.timestamps
    end
    add_index :developer_languages,
              %i[developer_id language_id],
              unique: true, name: 'dev_language'
  end
end
