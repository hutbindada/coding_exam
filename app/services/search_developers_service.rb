class SearchDevelopersService
  def initialize(params)
    @developer_email = params[:developer_email]
    @language = params[:language]
    @programming_language = params[:programming_language]
  end

  def perform
    conditions = []
    conditions << "email LIKE '%#{developer_email}%'" if developer_email.present?
    conditions << "languages.id IN ('#{language}')" if language.present?
    conditions << "programming_languages.id IN ('#{programming_language}')" if programming_language.present?
    Developer.joins(
      :languages,
      :programming_languages
    ).where(conditions.join(' AND '))
  end

  private

  attr_reader :developer_email, :language, :programming_language
end
