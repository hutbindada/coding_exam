module ApplicationHelper
  def active_class(ctr_name)
    [ctr_name].index(params[:controller]).present? ? 'is-tab-selected' : ''
  end
end
