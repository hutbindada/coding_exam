class LanguageSerializer < ActiveModel::Serializer
  attributes :id, :code
end
