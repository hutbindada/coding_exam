class ProgrammingLanguageSerializer < ActiveModel::Serializer
  attributes :id, :name
end
