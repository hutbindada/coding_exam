class DevelopersController < ApplicationController

  def index
    @entities = Developer.joins(:languages, :programming_languages).page params[:page]
  end

  def create_dummy_data
    `bundle exec rake developers:generate_dummy`
    redirect_to developers_path
  end

  def filter_data
    @entities = SearchDevelopersService.new(params_search).perform
    @entities = @entities.page params[:page]
    render :index
  end

  def destroy_all
    Developer.destroy_all
    redirect_to developers_path
  end

  private

  def params_search
    params.require(:filter).permit!
  end
end
