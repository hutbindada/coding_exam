module CRUD
  extend ActiveSupport::Concern

  included do
    before_action :set_entity, only: %i[edit update destroy]
  end

  def index
    @entities = entity_class.all.page params[:page]
  end

  def new
    @entity = entity_class.new
  end

  def edit; end

  def create
    @entity = entity_class.new(entity_params)
    if @entity.save
      redirect_to entity_class
    else
      render :new
    end
  end

  def update
    if @entity.update_attributes(entity_params)
      redirect_to entity_class
    else
      render :edit
    end
  end

  def destroy
    @entity.destroy
    redirect_back(fallback_location: request.referrer)
  end

  protected

  def entity_class
    @entity_class ||= controller_name.classify.constantize
  end

  def entity_params
    params.require(table_name.singularize.to_sym).permit!
  end

  def table_name
    entity_class.table_name
  end

  def set_entity
    @entity = entity_class.find(params[:id])
  end
end
