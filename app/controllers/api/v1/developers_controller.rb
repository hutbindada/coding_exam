module Api::V1
  class DevelopersController < ApiController
    def show
      developer = Developer.find(params[:id])
      render json: developer
    end
  end
end
