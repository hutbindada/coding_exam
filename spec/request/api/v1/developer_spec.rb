require 'rails_helper'
describe DevelopersController, type: :controller do
  before do
    @developer = build(:developer)

  end
  it 'retrieves a specific developer' do

    get "api/v1/developer/#{@developer.id}", format: :json

    json = JSON.parse(response.body)
    # test for the 200 status-code
    expect(response).to be_success
    # check that the message attributes are the same.
    expect(json['email']).to eq(@developer.email)

    # ensure that private attributes aren't serialized
    expect(json['private_attr']).to eq(nil)
  end
end
