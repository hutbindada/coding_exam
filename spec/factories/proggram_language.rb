FactoryGirl.define do
  factory :program_language do
    name       { Faker::Internet.name }
  end
end
