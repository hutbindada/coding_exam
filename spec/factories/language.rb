FactoryGirl.define do
  factory :language do
    code       { Faker::Internet.code }
  end
end
