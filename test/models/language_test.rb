require 'test_helper'

class LanguageTest < ActiveSupport::TestCase
  test "code can't be blank" do
    language = Language.new
    assert language.invalid?
    assert language.errors[:code].any?
  end
end
