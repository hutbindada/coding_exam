require 'test_helper'

class ProgrammingLanguageTest < ActiveSupport::TestCase
  test "name can't be blank" do
    p_language = ProgrammingLanguage.new
    assert p_language.invalid?
    assert p_language.errors[:name].any?
  end

  test 'name unique' do
    ProgrammingLanguage.create(name: 'firstname')
    p_language = ProgrammingLanguage.new(name: 'firstname')
    assert p_language.invalid?
    assert p_language.errors[:name].any?
  end
end
