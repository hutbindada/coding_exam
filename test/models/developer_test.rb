require 'test_helper'

class DeveloperTest < ActiveSupport::TestCase
  test "Email can't be blank" do
    developer = Developer.new
    assert developer.invalid?
    assert developer.errors[:email].any?
  end

  test 'Email unique' do
    Developer.create(email: 'first@gmail.com')
    developer = Developer.new(email: 'first@gmail.com')
    assert developer.invalid?
    assert developer.errors[:email].any?
  end
end
