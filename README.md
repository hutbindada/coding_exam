# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version: 2.4.1

* System dependencies

* Configuration

* Database creation:
  Create in your database.yml

* Database initialization
  bundle exec rake db:create
  bundle exec rake db:migrate

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)
  Run rake task by command: `bundle exec rake developers:generate_dummy`
  or
  Click button `Create dummy developers` home pages

* Deployment instructions

* Step:
  - Create database
  - bundle install
  - Go to home page, go to tab languages and programming languages for create data
  - Then create dummy data developers
  - Test api with routes `/api/v1/developers/:developer_id` by cURL in terminal or postman
  - Test search function in home page or in console by call Service object
