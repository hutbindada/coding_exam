Rails.application.routes.draw do
  root to: 'developers#index'
  resources :developers, except: :show do
    collection do
      post :create_dummy_data
      get :filter_data
      delete :destroy_all
    end
  end
  resources :languages, except: :show
  resources :programming_languages, except: :show
  namespace :api do
    namespace :v1 do
      resources :developers, only: :show
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
